# etherless-smart

## References

https://www.math.unipd.it/~tullio/IS-1/2019/Progetto/C2.pdf

Software Engineering project at University of Padua, developed for [Red Babel](http://redbabel.com/), by B.smart.

## Build
Build using

```sh
npm install && npm run build
```

## Testing

Local tests can be run using
```sh
npm run test
```