/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {ethers} from "ethers";

import {EtherlessFactory} from "./contracts/EtherlessFactory";

import {EthereumSearchTimedOutException} from "./exceptions/EthereumSearchTimedOutException";

import {SoftwareExecutionEventNotFoundException} from "./exceptions/SoftwareExecutionEventNotFoundException";

import {SoftwarePublicationEventNotFoundException} from "./exceptions/SoftwarePublicationEventNotFoundException";

import {SoftwareRemovalEventNotFoundException} from "./exceptions/SoftwareRemovalEventNotFoundException";

import {SoftwareUpdateEventNotFoundException} from "./exceptions/SoftwareUpdateEventNotFoundException";

import {Token} from "./Token";

import {
    EtherlessSmartReadOnlyInterface,
    EthereumConnectionConfigurationInterface,
    EtherlessSoftwareOperationInterface,
    EtherlessSoftwareDeployOperation,
    EtherlessSoftwareUpgradeOperation,
    EtherlessSoftwareRemovalOperation,
    EtherlessSoftwareExecutionOperation,
    SoftwareDetails,
    EthereumAmount
} from "@b-smart/etherless-types";

/*
    This class represents the Etherless smart contract and possible interactions.
*/
export class EtherlessSmartReadOnly implements EtherlessSmartReadOnlyInterface {

    readonly connectionConfiguration: EthereumConnectionConfigurationInterface;

    constructor(connectionConfiguration: EthereumConnectionConfigurationInterface) {
        this.connectionConfiguration = connectionConfiguration;
    }

    protected isNotGanache(): boolean {
        return (typeof this.connectionConfiguration.infuraNetwork === "string") &&
            (this.connectionConfiguration.infuraNetwork.length > 0) &&
            (this.connectionConfiguration.infuraNetwork !== "*") &&
            (
                (typeof this.connectionConfiguration.rpcProviderAddress !== "string") ||
                (this.connectionConfiguration.rpcProviderAddress.length === 0)
            );
    }

    protected getProvider(): ethers.providers.Provider {
        const provider = (this.isNotGanache())
            ? new ethers.providers.InfuraProvider(this.connectionConfiguration.infuraNetwork, this.connectionConfiguration.infuraProjectID)
            : new ethers.providers.JsonRpcProvider(this.connectionConfiguration.rpcProviderAddress);

        provider.resetEventsBlock(this.connectionConfiguration.resetToBlockNumber);

        return provider;
    }

    protected getSigner(): ethers.Signer {
        const randomWallet = ethers.Wallet.createRandom();

        return new ethers.Wallet(randomWallet.privateKey, this.getProvider());
    }

    public async getBlockNumber(): Promise<number> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        return contract.provider.getBlockNumber();
    }

    public async getAddress(): Promise<string> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        return contract.addressPromise;
    }

    private eventEncoder = (fromAddress: string, softwareName: string, changeType: ethers.utils.BigNumberish, token: string): EtherlessSoftwareOperationInterface => {
        switch (changeType.valueOf()) {
        case 0:
            return new EtherlessSoftwareDeployOperation(fromAddress, softwareName, new Token(token));
        case 1:
            return new EtherlessSoftwareUpgradeOperation(fromAddress, softwareName, new Token(token));
        case 2:
            return new EtherlessSoftwareRemovalOperation(fromAddress, softwareName, new Token(token));
        case 3:
            return new EtherlessSoftwareExecutionOperation(fromAddress, softwareName, new Token(token));
        default:
            throw undefined;
        }
    }

    private async collectEvents(stopCondition: (softwareOperation: EtherlessSoftwareOperationInterface) => boolean, timeout: number|undefined = undefined): Promise<Array<EtherlessSoftwareOperationInterface>> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        return new Promise((accept, reject) => {
            const filter = contract.filters.SoftwareOperationEvent(
                null,
                null,
                null,
                null
            );

            const pastEvents: Array<EtherlessSoftwareOperationInterface> = [];

            const stopPolling: () => void = () => {
                contract.removeAllListeners(filter);
                contract.provider.removeAllListeners(filter);
            };

            const timeoutTimer: NodeJS.Timeout|undefined = (typeof timeout === "number")
                ? setTimeout(() => {
                    stopPolling();
                    reject(new EthereumSearchTimedOutException());
                }, timeout)
                : undefined;
            
            const listener = (fromAddress: string, softwareName: string, changeType: ethers.utils.BigNumberish, token: string): void => {
                try {
                    const softwareOperation = this.eventEncoder(fromAddress, softwareName, changeType, token);
                    pastEvents.push(softwareOperation);
                    
                    // end polling if the stop condition has been satisfied
                    if (stopCondition(softwareOperation)) {
                        stopPolling();
    
                        if (typeof timeoutTimer !== "undefined") {
                            clearTimeout(timeoutTimer);
                        }

                        accept(pastEvents);
                    }
                } catch (exception) {
                    // Do nothing
                }
            };
            
            contract.on(filter, listener);
        });
    }

    public async getFirstSoftwarePublicationEvent(from: string|undefined = undefined, name: string|undefined = undefined, token: Token|undefined = undefined, timeout: number|undefined = undefined): Promise<EtherlessSoftwareDeployOperation> {
        const filter = (softwareOperation: EtherlessSoftwareOperationInterface): boolean => {
            if (!(softwareOperation instanceof EtherlessSoftwareDeployOperation)) {
                return false;
            } else if ((typeof from !== "undefined") && (softwareOperation.address.localeCompare(from) !== 0)) {
                return false;
            } else if ((typeof name !== "undefined") && (softwareOperation.name.localeCompare(name) !== 0)) {
                return false;
            } else if ((typeof token !== "undefined") && (token.toString().localeCompare(softwareOperation.token.toString()) !== 0)) {
                return false;
            }

            return true;
        };
        
        const softwareOperations = await this.collectEvents(filter, timeout);
        
        if (softwareOperations.length < 1) {
            throw new SoftwarePublicationEventNotFoundException();
        }

        return softwareOperations.filter(filter)[0] as EtherlessSoftwareDeployOperation;
    }

    public async getFirstSoftwareUpdateEvent(from: string|undefined = undefined, name: string|undefined = undefined, token: Token|undefined = undefined, timeout: number|undefined = undefined): Promise<EtherlessSoftwareUpgradeOperation> {
        const filter = (softwareOperation: EtherlessSoftwareOperationInterface): boolean => {
            if (!(softwareOperation instanceof EtherlessSoftwareUpgradeOperation)) {
                return false;
            } else if ((typeof from !== "undefined") && (softwareOperation.address.localeCompare(from) !== 0)) {
                return false;
            } else if ((typeof name !== "undefined") && (softwareOperation.name.localeCompare(name) !== 0)) {
                return false;
            } else if ((typeof token !== "undefined") && (token.toString().localeCompare(softwareOperation.token.toString()) !== 0)) {
                return false;
            }
            return true;
        };
        
        const softwareOperations = await this.collectEvents(filter, timeout);
        
        if (softwareOperations.length < 1) {
            throw new SoftwareUpdateEventNotFoundException();
        }

        return softwareOperations.filter(filter)[0] as EtherlessSoftwareDeployOperation;
    }

    public async getFirstSoftwareRemovalEvent(from: string|undefined = undefined, name: string|undefined = undefined, token: Token|undefined = undefined, timeout: number|undefined = undefined): Promise<EtherlessSoftwareRemovalOperation> {
        const filter = (softwareOperation: EtherlessSoftwareOperationInterface): boolean => {
            if (!(softwareOperation instanceof EtherlessSoftwareRemovalOperation)) {
                return false;
            } else if ((typeof from !== "undefined") && (softwareOperation.address.localeCompare(from) !== 0)) {
                return false;
            } else if ((typeof name !== "undefined") && (softwareOperation.name.localeCompare(name) !== 0)) {
                return false;
            } else if ((typeof token !== "undefined") && (token.toString().localeCompare(softwareOperation.token.toString()) !== 0)) {
                return false;
            }
            return true;
        };
        
        const softwareOperations = await this.collectEvents(filter, timeout);
        
        if (softwareOperations.length < 1) {
            throw new SoftwareRemovalEventNotFoundException();
        }

        return softwareOperations.filter(filter)[0] as EtherlessSoftwareDeployOperation;
    }

    public async getFirstSoftwareExecutionEvent(from: string|undefined = undefined, name: string|undefined = undefined, token: Token|undefined = undefined, timeout: number|undefined = undefined): Promise<EtherlessSoftwareExecutionOperation> {
        const filter = (softwareOperation: EtherlessSoftwareOperationInterface): boolean => {
            if (!(softwareOperation instanceof EtherlessSoftwareExecutionOperation)) {
                return false;
            } else if ((typeof from !== "undefined") && (softwareOperation.address.localeCompare(from) !== 0)) {
                return false;
            } else if ((typeof name !== "undefined") && (softwareOperation.name.localeCompare(name) !== 0)) {
                return false;
            } else if ((typeof token !== "undefined") && (token.toString().localeCompare(softwareOperation.token.toString()) !== 0)) {
                return false;
            }
            return true;
        };
        
        const softwareOperations = await this.collectEvents(filter, timeout);
        
        if (softwareOperations.length < 1) {
            throw new SoftwareExecutionEventNotFoundException();
        }

        return softwareOperations.filter(filter)[0] as EtherlessSoftwareDeployOperation;
    }

    public async getSoftwareList(): Promise<Array<Promise<SoftwareDetails>>> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        const lastToken = await contract.getLastToken();

        // This method can be called only once and only on non-empty contracts
        if (lastToken.toLowerCase() === "0xc5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470") {
            return [];
        }

        const softwareOperations = await await this.collectEvents((softwareOperation: EtherlessSoftwareOperationInterface): boolean => {
            return lastToken.localeCompare(softwareOperation.token.toString()) === 0;
        }, undefined);

        let softwareList: Array<string> = [];

        softwareOperations.forEach((operation: EtherlessSoftwareOperationInterface) => {
            if (operation instanceof EtherlessSoftwareDeployOperation) {
                softwareList.push(operation.name);
            } else if (operation instanceof EtherlessSoftwareRemovalOperation) {
                softwareList = softwareList.filter((softwareName: string) => {
                    return softwareName !== operation.name;
                });
            }
        });

        const softwareCollection: Array<Promise<SoftwareDetails>> = [];
        softwareList.forEach((softwareName: string) => softwareCollection.push(this.getSoftwareDetails(softwareName)));

        return softwareCollection;
    }

    public async getAllSoftwareExecutionByAddress(from: string): Promise<Array<EtherlessSoftwareExecutionOperation>> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        const lastToken = await contract.getLastToken();

        // This method can be called only once and only on non-empty contracts
        if (lastToken.toLowerCase() === "0xc5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470") {
            return [];
        }

        const softwareOperations = await await this.collectEvents((softwareOperation: EtherlessSoftwareOperationInterface): boolean => {
            return lastToken.localeCompare(softwareOperation.token.toString()) === 0;
        }, undefined);

        const executionsList: Array<EtherlessSoftwareExecutionOperation> = [];

        softwareOperations.forEach((operation: EtherlessSoftwareOperationInterface) => {
            if (operation instanceof EtherlessSoftwareExecutionOperation && operation.address === from) {
                executionsList.push(operation);
            }
        });

        return executionsList;
    }

    public async softwareExists(name: string): Promise<boolean> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        return contract.softwareExists(name);
    }

    public async getSoftwareDetails(name: string): Promise<SoftwareDetails> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        const softwareRawDetails = await contract.softwares(name);

        return new SoftwareDetails(
            name,
            new Date(1000 * softwareRawDetails.publicationTime.toNumber()),
            new Date(1000 * softwareRawDetails.lastUpdatedTime.toNumber()),
            parseInt(softwareRawDetails.updates.toString()) - 1,
            new EthereumAmount(softwareRawDetails.executionPrice.toString(), "wei"),
            softwareRawDetails.publisher
        );
    }

    public async getBalance(address: string): Promise<EthereumAmount> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        const amount = await contract.wallet(address);

        return new EthereumAmount(amount.toString(), "wei");
    }
}