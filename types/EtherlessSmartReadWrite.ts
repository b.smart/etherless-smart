/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {ethers} from "ethers";
import {EthereumAccount} from "./EthereumAccount";
import {EthereumGas, EthereumConnectionConfigurationInterface, EtherlessSmartReadWriteInterface, EthereumAmount} from "@b-smart/etherless-types";
import {EtherlessFactory} from "./contracts/EtherlessFactory";
import {TransactionOverrides} from "./contracts";
import {EtherlessSmartReadOnly} from "./EtherlessSmartReadOnly";
import {EthereumException} from "./exceptions/EthereumException";
import {Token} from "./Token";

import getRevertReason = require("eth-revert-reason")

/*
    This class represents the Etherless smart contract and possible interactions.
*/
export class EtherlessSmartReadWrite extends EtherlessSmartReadOnly implements EtherlessSmartReadWriteInterface {

    public static async deploy(connectionConfiguration: EthereumConnectionConfigurationInterface, account: EthereumAccount): Promise<string> {

        const provider: ethers.providers.Provider = (connectionConfiguration.rpcProviderAddress.length > 0)
            ? new ethers.providers.JsonRpcProvider(connectionConfiguration.rpcProviderAddress)
            : new ethers.providers.InfuraProvider(connectionConfiguration.infuraNetwork, connectionConfiguration.infuraProjectID);

        const wallet = new ethers.Wallet(account.privateKey, provider);

        const contractFactory: EtherlessFactory = new EtherlessFactory(wallet);
        const result = await contractFactory.deploy();

        return result.addressPromise;
    }

    public account: EthereumAccount;

    constructor(connectionConfiguration: EthereumConnectionConfigurationInterface, account: EthereumAccount) {
        super(connectionConfiguration);

        this.account = account;
    }

    protected getSigner(): ethers.Signer {
        return new ethers.Wallet(this.account.privateKey, this.getProvider());
    }

    public async initialize(gas: EthereumGas|undefined = undefined): Promise<void> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        // All overrides are optional
        const overrides: TransactionOverrides = (typeof gas !== "undefined")
            ? {
            // The maximum units of gas for the transaction to use
                gasLimit: gas.gasLimit,

                // The price (in wei) per unit of gas
                gasPrice: ethers.utils.parseUnits(gas.priceValue, gas.priceUnit)
            }
            : {
                gasPrice: await contract.provider.getGasPrice(),

                gasLimit: await contract.estimate.initialize()
            };

        try {
            const contractTransaction: ethers.ContractTransaction = await contract.initialize(overrides);
            /* const contractReceipt: ContractReceipt = */await contractTransaction.wait();
        } catch (ex) {
            if (this.isNotGanache()) {
                const message = await getRevertReason(ex.transactionHash, this.connectionConfiguration.infuraNetwork);
                throw new EthereumException(message);
            } else {
                throw new EthereumException(ex.message);
            }
        }
    }

    public async deploySoftware(name: string, token: Token, gas: EthereumGas|undefined = undefined): Promise<void> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        // All overrides are optional
        const overrides = (typeof gas !== "undefined")
            ? {
            // The maximum units of gas for the transaction to use
                gasLimit: gas.gasLimit,

                // The price (in wei) per unit of gas
                gasPrice: ethers.utils.parseUnits(gas.priceValue, gas.priceUnit)
            }
            : {
                gasPrice: await contract.provider.getGasPrice(),

                gasLimit: await contract.estimate.deploy(name, token.toString())
            };

        try {
            const contractTransaction: ethers.ContractTransaction = await contract.deploy(name, token.toString(), overrides);
            /* const contractReceipt: ContractReceipt = */ await contractTransaction.wait();
        } catch (ex) {
            if (this.isNotGanache()) {
                const message = await getRevertReason(ex.transactionHash, this.connectionConfiguration.infuraNetwork);
                throw new EthereumException(message);
            } else {
                throw new EthereumException(ex.message);
            }
        }
    }

    public async updateSoftware(name: string, token: Token, gas: EthereumGas|undefined = undefined): Promise<void> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        // All overrides are optional
        const overrides = (typeof gas !== "undefined")
            ? {
            // The maximum units of gas for the transaction to use
                gasLimit: gas.gasLimit,

                // The price (in wei) per unit of gas
                gasPrice: ethers.utils.parseUnits(gas.priceValue, gas.priceUnit)
            }
            : {
                gasPrice: await contract.provider.getGasPrice(),

                gasLimit: await contract.estimate.update(name, token.toString())
            };

        try {
            const contractTransaction: ethers.ContractTransaction = await contract.update(name, token.toString(), overrides);
            /* const contractReceipt: ContractReceipt = */ await contractTransaction.wait();
        } catch (ex) {
            if (this.isNotGanache()) {
                const message = await getRevertReason(ex.transactionHash, this.connectionConfiguration.infuraNetwork);
                throw new EthereumException(message);
            } else {
                throw new EthereumException(ex.message);
            }
        }
    }

    public async removeSoftware(name: string, token: Token, gas: EthereumGas|undefined = undefined): Promise<void> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        // All overrides are optional
        const overrides = (typeof gas !== "undefined")
            ? {
            // The maximum units of gas for the transaction to use
                gasLimit: gas.gasLimit,

                // The price (in wei) per unit of gas
                gasPrice: ethers.utils.parseUnits(gas.priceValue, gas.priceUnit)
            }
            : {
                gasPrice: await contract.provider.getGasPrice(),

                gasLimit: await contract.estimate.remove(name, token.toString())
            };
        
        try {
            const contractTransaction: ethers.ContractTransaction = await contract.remove(name, token.toString(), overrides);
            /* const contractReceipt: ContractReceipt = */ await contractTransaction.wait();
        } catch (ex) {
            if (this.isNotGanache()) {
                const message = await getRevertReason(ex.transactionHash, this.connectionConfiguration.infuraNetwork);
                throw new EthereumException(message);
            } else {
                throw new EthereumException(ex.message);
            }
        }
    }

    public async invokeSoftware(name: string, token: Token, gas: EthereumGas|undefined = undefined): Promise<void> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        // All overrides are optional
        const overrides = (typeof gas !== "undefined")
            ? {
            // The maximum units of gas for the transaction to use
                gasLimit: gas.gasLimit,

                // The price (in wei) per unit of gas
                gasPrice: ethers.utils.parseUnits(gas.priceValue, gas.priceUnit)
            }
            : {
                gasPrice: await contract.provider.getGasPrice(),

                gasLimit: await contract.estimate.execute(name, token.toString())
            };

        try {
            const contractTransaction: ethers.ContractTransaction = await contract.execute(name, token.toString(), overrides);
            /* const contractReceipt: ContractReceipt = */ await contractTransaction.wait();
        } catch (ex) {
            if (this.isNotGanache()) {
                const message = await getRevertReason(ex.transactionHash, this.connectionConfiguration.infuraNetwork);
                throw new EthereumException(message);
            } else {
                throw new EthereumException(ex.message);
            }
        }
    }

    public async deposit(amount: EthereumAmount, gas: EthereumGas|undefined = undefined): Promise<void> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        const parsedAmount = ethers.utils.parseUnits(amount.priceValue, amount.priceUnit);

        // All overrides are optional
        const overrides = (typeof gas !== "undefined")
            ? {
            // The maximum units of gas for the transaction to use
                gasLimit: gas.gasLimit,

                // The price (in wei) per unit of gas
                gasPrice: ethers.utils.parseUnits(gas.priceValue, gas.priceUnit),

                value: parsedAmount
            }
            : {
                gasPrice: await contract.provider.getGasPrice(),

                gasLimit: await contract.estimate.send(),

                value: parsedAmount
            };
        
        try {
            const contractTransaction: ethers.ContractTransaction = await contract.send(overrides);
            /* const contractReceipt: ContractReceipt = */ await contractTransaction.wait();
        } catch (ex) {
            if (this.isNotGanache()) {
                const message = await getRevertReason(ex.transactionHash, this.connectionConfiguration.infuraNetwork);
                throw new EthereumException(message);
            } else {
                throw new EthereumException(ex.message);
            }
        }
    }

    public async withdraw(gas: EthereumGas|undefined = undefined): Promise<void> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        // All overrides are optional
        const overrides = (typeof gas !== "undefined")
            ? {
            // The maximum units of gas for the transaction to use
                gasLimit: gas.gasLimit,

                // The price (in wei) per unit of gas
                gasPrice: ethers.utils.parseUnits(gas.priceValue, gas.priceUnit)
            }
            : {
                gasPrice: await contract.provider.getGasPrice(),

                gasLimit: await contract.estimate.withdraw()
            };
        
        try {
            const contractTransaction: ethers.ContractTransaction = await contract.withdraw(overrides);
            /* const contractReceipt: ContractReceipt = */ await contractTransaction.wait();
        } catch (ex) {
            if (this.isNotGanache()) {
                const message = await getRevertReason(ex.transactionHash, this.connectionConfiguration.infuraNetwork);
                throw new EthereumException(message);
            } else {
                throw new EthereumException(ex.message);
            }
        }
    }

    public async changeSoftwarePrice(name: string, amount: EthereumAmount, gas: EthereumGas|undefined = undefined): Promise<void> {
        const contractFactory: EtherlessFactory = new EtherlessFactory(this.getSigner());
        const contract = contractFactory.attach(this.connectionConfiguration.smartAddress);
        contract.connect(this.getSigner());

        const parsedAmount = ethers.utils.parseUnits(amount.priceValue, amount.priceUnit);

        // All overrides are optional
        const overrides = (typeof gas !== "undefined")
            ? {
            // The maximum units of gas for the transaction to use
                gasLimit: gas.gasLimit,

                // The price (in wei) per unit of gas
                gasPrice: ethers.utils.parseUnits(gas.priceValue, gas.priceUnit)
            }
            : {
                gasPrice: await contract.provider.getGasPrice(),

                gasLimit: await contract.estimate.changeExecutionPrice(name, parsedAmount)
            };
        
        try {
            const contractTransaction: ethers.ContractTransaction = await contract.changeExecutionPrice(name, parsedAmount,overrides);
            /* const contractReceipt: ContractReceipt = */ await contractTransaction.wait();
        } catch (ex) {
            if (this.isNotGanache()) {
                const message = await getRevertReason(ex.transactionHash, this.connectionConfiguration.infuraNetwork);
                throw new EthereumException(message);
            } else {
                throw new EthereumException(ex.message);
            }
        }
    }
}