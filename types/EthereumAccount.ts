/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {EthereumAccountInterface} from "@b-smart/etherless-types";
import {InvalidPrivateKeyException} from "./exceptions/InvalidPrivateKeyException";
import {ethers} from "ethers";
import {DerivedKey} from "./DerivedKey";
import * as sha256 from "fast-sha256";

/**
 * Represents an Ethereum wallet to be used while interacting with Etherless.
 */
export class EthereumAccount implements EthereumAccountInterface {

    public readonly privateKey: string;

    constructor(privateKey: string) {
        if (!(privateKey.length === 66 && /^0x[0-9|A-F|a-f]*$/.test(privateKey))) {
            throw new InvalidPrivateKeyException(privateKey);
        }

        this.privateKey = privateKey;
    }

    public withPrivateKey(privateKey: string): EthereumAccount {
        return new EthereumAccount(privateKey);
    }

    public async getAddress(): Promise<string> {
        return ethers.utils.computeAddress(this.privateKey);
    }

    public static generatePrivateKey(): string {
        return ethers.Wallet.createRandom().privateKey;
    }

    public async getPrivateKeyHash(): Promise<DerivedKey> {
        const hasher = new sha256.Hash();
        hasher.update((new TextEncoder()).encode(this.privateKey));
        const result = hasher.digest();

        return new DerivedKey(`0x${[...result].map (b => b.toString(16).padStart(2, "0")).join ("")}`);
    }

}