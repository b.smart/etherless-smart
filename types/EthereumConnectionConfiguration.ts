/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {EthereumConnectionConfigurationInterface} from "@b-smart/etherless-types";

export class EthereumConnectionConfiguration implements EthereumConnectionConfigurationInterface {

    readonly infuraProjectID: string

    readonly infuraNetwork: string

    readonly smartAddress: string

    readonly resetToBlockNumber: number

    readonly rpcProviderAddress: string

    constructor(
        infuraProjectID = "",
        infuraNetwork = "",
        rpcProviderAddress = "",
        smartAddress = "",
        resetToBlockNumber = 0
    ) {
        this.infuraProjectID = infuraProjectID;
        this.infuraNetwork = infuraNetwork;
        this.smartAddress = smartAddress;
        this.resetToBlockNumber = resetToBlockNumber;
        this.rpcProviderAddress = rpcProviderAddress;
    }

    public withRPCProviderAddress(rpcProviderAddress: string): EthereumConnectionConfiguration {
        return new EthereumConnectionConfiguration(
            this.infuraProjectID,
            this.infuraNetwork,
            rpcProviderAddress,
            this.smartAddress,
            this.resetToBlockNumber,
        );
    }

    public withInfuraProjectID(infuraProjectID: string): EthereumConnectionConfiguration {
        return new EthereumConnectionConfiguration(
            infuraProjectID,
            this.infuraNetwork,
            this.rpcProviderAddress,
            this.smartAddress,
            this.resetToBlockNumber,
        );
    }

    public withInfuraNetwork(infuraNetwork: string): EthereumConnectionConfiguration {
        return new EthereumConnectionConfiguration(
            this.infuraProjectID,
            infuraNetwork,
            this.rpcProviderAddress,
            this.smartAddress,
            this.resetToBlockNumber,
        );
    }

    public withSmartAddress(smartAddress: string): EthereumConnectionConfiguration {
        return new EthereumConnectionConfiguration(
            this.infuraProjectID,
            this.infuraNetwork,
            this.rpcProviderAddress,
            smartAddress,
            this.resetToBlockNumber,
        );
    }

    public withFirstBlockNumber(resetToBlockNumber: number): EthereumConnectionConfiguration {
        return new EthereumConnectionConfiguration(
            this.infuraProjectID,
            this.infuraNetwork,
            this.rpcProviderAddress,
            this.smartAddress,
            resetToBlockNumber,
        );
    }

}