/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { accounts, contract } from '@openzeppelin/test-environment'
import 'mocha'
import { expect } from 'chai'

import { randomBytes } from 'crypto'

// Import utilities from Test Helpers
const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers')

const Etherless = contract.fromArtifact('Etherless')

function getRandomToken(): string {
  return "0x" + [...new Uint8Array(randomBytes(32))]
  .map (b => b.toString(16).padStart(2, "0"))
  .join ("")
}

describe('Etherless', function () {
  // One minute timeout
  this.timeout(60 * 1000)

  const [ owner, other ] = accounts

  beforeEach(async function () {
    this.contract = await Etherless.new({ from: owner })
    await this.contract.initialize({from: owner })
  })

  it('correctly detects published software', async function () {
    const testSoftwareName = "swTest2"

    // A software with that name shouldn't exists
    const softwareExistanceBeforeDeploy = await this.contract.softwareExists(testSoftwareName)
    expect(softwareExistanceBeforeDeploy).to.be.deep.eq(false)
    
    // test the creation of a new software
    await this.contract.deploy(testSoftwareName, getRandomToken(), {from: accounts[0]})

    // A software with that name should exists now!
    const softwareExistanceAfterDeploy = await this.contract.softwareExists(testSoftwareName)
    expect(softwareExistanceAfterDeploy).to.be.deep.eq(true)
  })

  it("emits corrects event when a software is published", async function() {
    const testSoftwareName = "swTest3"

    // test the creation of a new software
    const softwareDeployResult = await this.contract.deploy(testSoftwareName, getRandomToken(), {from: accounts[1]})

    // there should be a software publication event
    expectEvent(softwareDeployResult, 'SoftwareOperationEvent', {
      changerAddress: accounts[1],
      //softwareName: testSoftwareName,
      changeType: new BN(0),
    })
  })

  it("cannot overwrite a published software", async function() {
    const testSoftwareName = "regrfdgvf"

    // test the creation of a new software
    await this.contract.deploy(testSoftwareName, getRandomToken(), {from: other})

    // test the creation of a software with an existing name. An event should not be generated
    await expectRevert(
      this.contract.deploy(testSoftwareName, getRandomToken(), { from: other }),
      'Software already exists'
    )
  })

  it("emits corrects event when a software is removed", async function() {
    const testSoftwareName = "swTest5"
    const testToken = "fwdad234ujf983j9ihjnisuafdiuofesjoiua"

    await this.contract.deploy(testSoftwareName, getRandomToken(), {from: accounts[1]})

    const softwareRemoveResult = await this.contract.remove(testSoftwareName, getRandomToken(), {from: accounts[1]})

    expectEvent(softwareRemoveResult, 'SoftwareOperationEvent', {
      changerAddress: accounts[1],
      softwareName: testSoftwareName,
      changeType: new BN(2),
    })
  })

  it("emits corrects event when a software is updated", async function() {
    const testSoftwareName = "swTest6";
    const testToken = getRandomToken();

    await this.contract.deploy(testSoftwareName, testToken, {from: accounts[1]})

    const softwareUpdateResult = await this.contract.update(testSoftwareName, getRandomToken(), {from: accounts[1]})

    expectEvent(softwareUpdateResult, 'SoftwareOperationEvent', {
      changerAddress: accounts[1],
      softwareName: testSoftwareName,
      changeType: new BN(1),
    })
  })

  it("can remove a published software iif the request comes from the original publisher", async function() {
    const testSoftwareName = "softwareTest7";

    await this.contract.deploy(testSoftwareName, getRandomToken(), {from: accounts[1]})

    await expectRevert(
      this.contract.remove("Th1s s0ftw4r3 dznt 3x1sts", getRandomToken(), {from: accounts[1]}),
      'Software must exists to be removed'
    )

    await expectRevert(
      this.contract.remove(testSoftwareName, getRandomToken(), {from: accounts[0]}),
      'permission denied: only the original publisher can remove a software'
    )
    
    const removeResult = await this.contract.remove(testSoftwareName, getRandomToken(), {from: accounts[1]});
    expectEvent(removeResult, 'SoftwareOperationEvent', {
      changerAddress: accounts[1],
      softwareName: testSoftwareName,
      changeType: new BN(2),
    })
  })

  it("can update a published software if the request comes from the original publisher", async function() {
    const testSoftwareName = "softwareTest8";

    await this.contract.deploy(testSoftwareName, getRandomToken(), {from: accounts[1]});

    await expectRevert(
      this.contract.update("Th1s s0ftw4r3 dznt 3x1sts", getRandomToken(), {from: accounts[1]}),
      'Software must exists to be updated'
    )

    await expectRevert(
      this.contract.update(testSoftwareName, getRandomToken(), {from: accounts[0]}),
      'permission denied: only the original publisher can update a software'
    )

    const updateFromRightUserResult = await this.contract.update(testSoftwareName, getRandomToken(), {from: accounts[1]});
    expectEvent(updateFromRightUserResult, 'SoftwareOperationEvent', {
      changerAddress: accounts[1],
      softwareName: testSoftwareName,
      changeType: new BN(1),
    })
  })

  it("can update price", async function() {
    const testSoftwareName = "softwareTest9";

    await this.contract.deploy(testSoftwareName, getRandomToken(), {from: accounts[1]});

    await this.contract.changeExecutionPrice(testSoftwareName, "28000", {from: owner});

    var sw = await this.contract.softwares.call(testSoftwareName);
    var swPrice = sw[4].words[0];

    expect(swPrice).to.be.deep.eq(28000);
  })

  it("can't change the price of a software with a wrong name", async function() {
    const testSoftwareName = "softwareTest10";

    await this.contract.deploy(testSoftwareName, getRandomToken(), {from: accounts[1]});

    await expectRevert(
      this.contract.changeExecutionPrice(testSoftwareName + "A", "28000", {from: owner}),
      'Software must exists to have its price changed -- Reason given: Software must exists to have its price changed.'
    )
  })

  it("can't reduce the cost of a software to a value less than the global minimum price", async function() {
    const testSoftwareName = "softwareTest11";

    await this.contract.deploy(testSoftwareName, getRandomToken(), {from: accounts[1]});

    await expectRevert(
      this.contract.changeExecutionPrice(testSoftwareName, "100", {from: owner}),
      'Software execution cannot cost less than the global minimum price -- Reason given: Software execution cannot cost less than the global minimum price.'
    )
  })
  
  it("execute a software", async function() {
    const testSoftwareName = "softwareTest12";

    await this.contract.deploy(testSoftwareName, getRandomToken(), {from: accounts[1]})

    await this.contract.send({from: accounts[0], value: 27000});
    
    const executedResult = await this.contract.remove(testSoftwareName, getRandomToken(), {from: accounts[1]});
    expectEvent(executedResult, 'SoftwareOperationEvent', {
      changerAddress: accounts[1],
      softwareName: testSoftwareName,
      changeType: new BN(2),
    })
  })

  it("cannot execute a software, not enough coins", async function() {
    const testSoftwareName = "softwareTest12";

    await this.contract.deploy(testSoftwareName, getRandomToken(), {from: accounts[1]})

    await this.contract.send({from: accounts[0], value: 26000});

    await expectRevert(
      this.contract.execute(testSoftwareName, getRandomToken(), {from: accounts[0]}),
      'not enough coins -- Reason given: not enough coins.'
    )
  })

  it("cannot execute a software, software not existent", async function() {
    const testSoftwareName = "softwareTest13";

    await this.contract.send({from: accounts[0], value: 26000});

    await expectRevert(
      this.contract.execute(testSoftwareName, getRandomToken(), {from: accounts[0]}),
      'Software must exists to be executed'
    )
  })

  it("cannot execute a software, token already used", async function() {
    const testSoftwareName = "softwareTest14";
    const tokenToBeReused = getRandomToken();

    await this.contract.deploy(testSoftwareName, tokenToBeReused, {from: accounts[1]})

    await this.contract.send({from: accounts[0], value: 27000});

    await expectRevert(
      this.contract.execute(testSoftwareName, tokenToBeReused, {from: accounts[0]}),
      'Token has already been used'
    )
  })
})
    