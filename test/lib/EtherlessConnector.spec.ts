/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import 'mocha';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
chai.use(chaiAsPromised);
const expect = chai.expect;

import { EthereumException } from "../../types/exceptions/EthereumException";

import { EtherlessSmartReadWrite, EthereumAccount } from '../../index'
import { ethers } from 'ethers';
import {
  EtherlessSoftwareDeployOperation,
  EthereumConnectionConfigurationInterface,
  EthereumAmount,
  EthereumGas
} from '@b-smart/etherless-types';

import { Token } from '../../types/Token';
import { EthereumConnectionConfiguration } from '../../types/EthereumConnectionConfiguration';
import { EtherlessSmartReadOnly } from '../../types/EtherlessSmartReadOnly';

const connectionConfiguration: EthereumConnectionConfiguration = new EthereumConnectionConfiguration(
  "",
  "",
  "http://ganache-cli:8545",
  "",
  0
)

let contract: EtherlessSmartReadWrite[]
const account: EthereumAccount[] = [
  new EthereumAccount("0x4f3edf983ac636a65a842ce7c78d9aa706d3b113bce9c46f30d7d21715b23b1d"),
  new EthereumAccount("0x6cbed15c793ce57650b9877cf6fa156fbef513c4e6134f022a85b1ffdd59b2a1"),
  new EthereumAccount("0x6370fd033278c143179d81c5526140625662b8daa446c22ee2d73db3707e620c"),
  new EthereumAccount("0x646f1ce2fdad0e6deeeb5c7e8e5543bdde65e86029e2fd9fc169899c440a7913"),
  new EthereumAccount("0xadd53f9a7e588d003326d1cbf9e4a43c061aadd9bc938c843a79e7b4fd2ad743"),
  new EthereumAccount("0x395df67f0c2d2d9fe1ad08d1bc8b6627011959b79c53d7dd6a3536a33ab8a4fd"),
  new EthereumAccount("0xe485d098507f54e7733a205420dfddbe58db035fa577fc294ebd14db90767a52"),
  new EthereumAccount("0xa453611d9419d0e56f499079478fd72c37b251a94bfde4d19872c44cf65386e3"),
  new EthereumAccount("0x829e924fdf021ba3dbbc4225edfece9aca04b929d6e75613329ca6f1d31c0bb4"),
  new EthereumAccount("0xb0057716d5917badaf911b193b12b910811c1497b5bada8d7711f758981c3773"),
]

describe('EtherlessConnector', function () {
  // One minute timeout
  this.timeout(60 * 1000)

  before(async function () {
    const constractSmartAddress: string = await EtherlessSmartReadWrite.deploy(connectionConfiguration, account[0])

    contract = [
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[0]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[1]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[2]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[3]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[4]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[5]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[6]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[7]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[8]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[9]),
    ]

    await contract[0].initialize()
  })

  it('deploy + list: correctly deploy a software and list it ', async function () {
    const accountIndex = 6

    const token: Token = Token.generateToken()
    const softwareName = "testSoftware" + token

    const firstSearchResult = await contract[accountIndex].softwareExists(softwareName)
    expect(firstSearchResult).to.be.deep.eq(false)

    await contract[accountIndex].deploySoftware(softwareName, token)

    const event = await contract[0].getFirstSoftwarePublicationEvent(undefined, softwareName, token, undefined)
    expect(event.address).to.be.deep.eq(await account[accountIndex].getAddress())
    expect(event.name).to.be.deep.eq(softwareName)
    expect(event.token.toString()).to.be.deep.eq(token.toString())
    expect(event instanceof EtherlessSoftwareDeployOperation).to.be.deep.eq(true)

    const secondSearchResult = await contract[0].softwareExists(softwareName)
    expect(secondSearchResult).to.be.deep.eq(true)
  })

  it('deploy: correctly detects events', async function () {
    const accountIndex = 3

    const token = Token.generateToken()
    const softwareName = "testSoftware" + token

    expect("" + token).to.be.deep.eq(token.toString())

    await contract[accountIndex].deploySoftware(softwareName, token)

    const eventO = await contract[accountIndex].getFirstSoftwarePublicationEvent(await account[accountIndex].getAddress(), softwareName, token, undefined)
    expect(eventO.address).to.be.deep.eq(await account[accountIndex].getAddress())

    for (let i = 0; i < 10; ++i) {
      const event = await contract[i].getFirstSoftwarePublicationEvent(undefined, softwareName, token, undefined)
      expect(event.address).to.be.deep.eq(await account[accountIndex].getAddress())
      expect(event.name).to.be.deep.eq(softwareName)
      expect(event.token.toString()).to.be.deep.eq(token.toString())
      expect(event instanceof EtherlessSoftwareDeployOperation).to.be.deep.eq(true)
    }
  })

  it('should correctly list available software', async function () {
    const accountIndex = 6

    const softwares = [
      "testSoftware",
      "ncc",
      "iuajuofd",
      "i988j8j3f",
      "odawdwa-adw",
    ]

    await contract[9].deploySoftware(softwares[0], Token.generateToken())
    await contract[9].removeSoftware(softwares[0], Token.generateToken())
    await contract[9].deploySoftware(softwares[0], Token.generateToken())

    await contract[8].deploySoftware(softwares[1], Token.generateToken())
    await contract[8].removeSoftware(softwares[1], Token.generateToken())
    await contract[8].deploySoftware(softwares[1], Token.generateToken())
    await contract[8].removeSoftware(softwares[1], Token.generateToken())
    await contract[8].deploySoftware(softwares[1], Token.generateToken())
    await contract[8].removeSoftware(softwares[1], Token.generateToken())
    await contract[8].deploySoftware(softwares[1], Token.generateToken())

    await contract[7].deploySoftware(softwares[2], Token.generateToken())
    await contract[7].removeSoftware(softwares[2], Token.generateToken())
    await contract[7].deploySoftware(softwares[2], Token.generateToken())
    await contract[7].updateSoftware(softwares[2], Token.generateToken())

    await contract[6].deploySoftware(softwares[3], Token.generateToken())
    await contract[6].removeSoftware(softwares[3], Token.generateToken())
    await contract[6].deploySoftware(softwares[3], Token.generateToken())

    await contract[5].deploySoftware(softwares[4], Token.generateToken())
    await contract[5].removeSoftware(softwares[4], Token.generateToken())
    await contract[5].deploySoftware(softwares[4], Token.generateToken())

    const readonlyContract = new EtherlessSmartReadOnly(contract[0].connectionConfiguration)

    const softwareList = await readonlyContract.getSoftwareList()
    const resolvedSoftwareList = await Promise.all(softwareList);

    const names: Array<string> = [];
    resolvedSoftwareList.forEach((value) => {
      names.push(value.name)
    })

    softwares.forEach(value => {
      expect(names.indexOf(value)).to.be.greaterThan(0)
    })


  })

})

describe('InvokeSoftware', function () {

  this.timeout(60 * 1000);

  before(async function () {
    const constractSmartAddress: string = await EtherlessSmartReadWrite.deploy(connectionConfiguration, account[0])

    contract = [
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[0]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[1]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[2]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[3]),
    ]
    await contract[0].initialize();
  });

  it('correctly write deploy event', async function () {
    const accountIndex = 3

    const token: Token = Token.generateToken()
    const softwareName = "testSoftware" + token

    await contract[accountIndex].deploySoftware(softwareName, token)

    const deployEvent = await contract[accountIndex].getFirstSoftwarePublicationEvent(await account[accountIndex].getAddress(), softwareName, token, undefined)
    expect(deployEvent.address).to.be.deep.eq(await account[accountIndex].getAddress())

  });

  it('correctly write execute event', async function () {
    const accountIndex = 3
    const deployToken: Token = Token.generateToken();
    const softwareName = "testSoftware" + deployToken

    //Do per assodato abbia fatto il deploy 
    await contract[2].deploySoftware(softwareName, deployToken)

    await contract[3].deposit(new EthereumAmount("5000", "gwei"), new EthereumGas(3000000, "9.0", "gwei"));
    const invokeToken: Token = Token.generateToken();
    await contract[3].invokeSoftware(softwareName, invokeToken, new EthereumGas(3000000, "9.0", "gwei"));
    const executeEvent = await contract[3].getFirstSoftwareExecutionEvent(await account[3].getAddress(), softwareName, invokeToken, undefined);
    expect(executeEvent.address).to.be.deep.eq(await account[3].getAddress())

  });

})

describe('UpdateSoftware', function () {

  this.timeout(60 * 1000);

  before(async function () {
    const constractSmartAddress: string = await EtherlessSmartReadWrite.deploy(connectionConfiguration, account[0])

    contract = [
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[0]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[1]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[2]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[3]),
    ]
    await contract[0].initialize();
  });

  it('correctly update deployed software', async function () {
    const accountIndex = 3

    const deployToken: Token = Token.generateToken()
    const softwareName = "testSoftware" + deployToken

    await contract[accountIndex].deploySoftware(softwareName, deployToken)
    //software deployato

    const removeToken: Token = Token.generateToken()

    await contract[accountIndex].removeSoftware(softwareName, removeToken)
    const removeEvent = await contract[accountIndex].getFirstSoftwareRemovalEvent(await account[accountIndex].getAddress(), softwareName, removeToken, undefined)
    expect(removeEvent.address).to.be.deep.eq(await account[accountIndex].getAddress())

  });


});

describe('RemoveSoftware', function () {
  this.timeout(60 * 1000);

  before(async function () {
    const constractSmartAddress: string = await EtherlessSmartReadWrite.deploy(connectionConfiguration, account[0])

    contract = [
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[0]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[1]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[2]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[3]),
    ]
    await contract[1].initialize();// was 0
  });

  it('the remove event is written correctly if required by the original user (account 1)', async function () {
    const accountIndex = 1;
    const deployToken: Token = Token.generateToken();
    const softwareName = "testSoftware" + deployToken;
    await contract[accountIndex].deploySoftware(softwareName, deployToken);
    //software deployato

    const removeToken: Token = Token.generateToken();
    await contract[accountIndex].removeSoftware(softwareName, removeToken);

    const removeEvent = await contract[accountIndex].getFirstSoftwareRemovalEvent(await account[accountIndex].getAddress(), softwareName, removeToken, undefined)
    expect(removeEvent.address).to.be.deep.eq(await account[accountIndex].getAddress())
  });

  it('the remove event is refused if required by any other user (account 2)', async function () {
    const accountOwner = 1;
    const accountNotOwner = 2;
    const deployToken: Token = Token.generateToken();
    const softwareName = "testSoftware" + deployToken;
    await contract[accountOwner].deploySoftware(softwareName, deployToken);
    //software deployato

    const removeToken: Token = Token.generateToken();

    await expect(contract[accountNotOwner].removeSoftware(softwareName, removeToken)).to.be.rejectedWith(Error)
    // 'VM Exception while processing transaction: revert permission denied: only the original publisher can remove a software'
  });
});

describe('Deposit & Withdraw', function () {
  this.timeout(60 * 1000);

  before(async function () {
    const constractSmartAddress: string = await EtherlessSmartReadWrite.deploy(connectionConfiguration, account[0])

    contract = [
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[0]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[1]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[2]),
      new EtherlessSmartReadWrite(connectionConfiguration.withSmartAddress(constractSmartAddress), account[3]),
    ]
    await contract[0].initialize();
  });

  it('Deposit founds correctly', async function () {
    await contract[3].deposit(new EthereumAmount("5000", "wei"), new EthereumGas(3000000, "9.0", "gwei"));
    let DepositAmount: EthereumAmount = new EthereumAmount("0", "wei");
    DepositAmount = await contract[3].getBalance((await account[3].getAddress()).toString());
    expect(DepositAmount.toString()).to.be.deep.eq("5000 wei")

  })

  it('Withdraw founds correctly', async function () {
    await contract[3].withdraw(new EthereumGas(3000000, "9.0", "gwei"));
    let WithdrawAmount: EthereumAmount = await contract[3].getBalance((await account[3].getAddress()).toString());
    expect(WithdrawAmount.toString()).to.be.deep.eq("0 wei")
  })
})
