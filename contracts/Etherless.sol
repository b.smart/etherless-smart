//solium-disable linebreak-style

/*
 * MIT License
 *
 * Copyright (c) 2020 B.smart
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

//SPDX-License-Identifier: MIT

pragma solidity >=0.6.3 <0.7.0;

import "./includes/Initializable.sol";
import "./includes/Ownable.sol";
import "./includes/WalletContainer.sol";

contract Etherless is Initializable, /*Ownable,*/ WalletContainer {

    /**
     * @dev The minimum price for a software execution
     */
    uint private constant minPrice = 27000;

    struct Software {
        uint256 publicationTime;
        uint256 lastUpdatedTime;
        address publisher;
        uint32 updates;
        uint executionPrice;
    }

    /*
        changeType =>
            (0) PUBLICATION,
            (1) UPDATE,
            (2) REMOVAL,
            (3) EXECUTION
    */
    event SoftwareOperationEvent(
        address indexed changerAddress,
        string softwareName, //Otherwise with indexed it would be hashed with keccak256
        uint8 indexed changeType,
        bytes32 indexed token
    );

    mapping (string => Software) public softwares;

    mapping (bytes32 => bool) private usedTokens;

    bytes32 private lastToken;

    function getLastToken() public initializationRequired view returns (bytes32 retVal) {
        return lastToken;
    }

    function emitSoftwareEvent(string memory name, uint8 changeType, bytes32 token) private initializationRequired {
        usedTokens[token] = true;
        lastToken = token;

        // emit the event
        emit SoftwareOperationEvent(msg.sender, name, changeType, token);
    }

    function initialize() public initializer {
        // Set the owner of the contract
        owner = msg.sender;

        lastToken = keccak256(abi.encodePacked(""));
    }

    function softwareExists(string memory name) public initializationRequired view returns (bool retVal) {
        return (
            (softwares[name].publicationTime > 0) /*&&
            (softwares[name].updates >= 1) &&
            (softwares[name].lastUpdatedTime >= softwares[name].publicationTime) &&
            (softwares[name].publisher != address(0))*/
        );
    }

    function changeExecutionPrice(string memory name, uint256 newExecutionPrice) public ownerOnly {
        require (newExecutionPrice >= minPrice, "Software execution cannot cost less than the global minimum price");
        require (softwareExists(name), "Software must exists to have its price changed");

        softwares[name].executionPrice = newExecutionPrice;
    }

    function execute(string memory name, bytes32 token) public initializationRequired {
        require (!usedTokens[token], "Token has already been used");
        require (softwareExists(name), "Software must exists to be executed");

        userPays(msg.sender, softwares[name].executionPrice);

        emitSoftwareEvent(name, 3, token);
    }

    function remove(string memory name, bytes32 token) public initializationRequired {
        require (!usedTokens[token], "Token has already been used");
        require (softwareExists(name), "Software must exists to be removed");

        // Only the publisher of a software can remove it
        require(msg.sender == softwares[name].publisher, "permission denied: only the original publisher can remove a software");

        Software memory sw;
        sw.publisher = address(0);
        sw.publicationTime = 0;
        sw.lastUpdatedTime = 0;
        sw.updates = 0;
        sw.executionPrice = 0;

        softwares[name] = sw;

        // emit the removal event
        emitSoftwareEvent(name, 2, token);
    }

    function deploy(string memory name, bytes32 token) public initializationRequired {
        require (!usedTokens[token], "Token has already been used");
        require (!softwareExists(name), "Software already exists");

        Software memory sw;
        sw.publisher = msg.sender;
        sw.publicationTime = block.timestamp;
        sw.lastUpdatedTime = sw.publicationTime;
        sw.updates = 1;
        sw.executionPrice = minPrice;

        // Save the software
        softwares[name] = sw;

        // emit the pubblication event
        emitSoftwareEvent(name, 0, token);
    }

    function update(string memory name, bytes32 token) public initializationRequired {
        require (!usedTokens[token], "Token has already been used");
        require (softwareExists(name), "Software must exists to be updated");

        // Only the publisher of a software can update it
        require(msg.sender == softwares[name].publisher, "permission denied: only the original publisher can update a software");

        // effectively update the software
        softwares[name].lastUpdatedTime = block.timestamp;
        softwares[name].updates += 1;

        // emit the upgrade event
        emitSoftwareEvent(name, 1, token);
    }

    // Reserved storage space to allow for layout changes in the future.
    uint256[50] private ______gap;
}
