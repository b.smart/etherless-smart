//solium-disable linebreak-style
pragma solidity >=0.6.3 <0.7.0;

/**
 * @title Initializable
 *
 * @dev Helper contract to support initializer functions. To use it, replace
 * the constructor with a function that has the `initializer` modifier.
 * WARNING: Unlike constructors, initializer functions must be manually
 * invoked. This applies both to deploying an Initializable contract, as well
 * as extending an Initializable contract via inheritance.
 * WARNING: When used with inheritance, manual care must be taken to not invoke
 * a parent initializer twice, or ensure that all initializers are idempotent,
 * because this is not dealt with automatically as with constructors.
 */
contract Initializable {

  /**
   * @dev Indicates that the contract has been initialized.
   */
  bool private initialized;

  bool private initializing;

  /**
   * @dev Modifier to use in the initializer function of a contract.
   */
  modifier initializer() {
    require(!initialized && !initializing, "Contract instance has already been initialized");

    initializing = true;

    _;

    initialized = true;
    initializing = false;
  }

  modifier initializationRequired() {
    require(initialized, "Not initialized");

    _;
  }

  // Reserved storage space to allow for layout changes in the future.
  uint256[50] private ______gap;
}