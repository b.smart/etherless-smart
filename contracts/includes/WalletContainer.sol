//solium-disable linebreak-style

/*
 * MIT License
 *
 * Copyright (c) 2020 B.smart
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

//SPDX-License-Identifier: MIT

pragma solidity >=0.6.3 <0.7.0;

//solium-disable linebreak-style

import "./Ownable.sol";

contract WalletContainer is Ownable {

    mapping (address => uint) public wallet;

    function userPays(address user, uint amount) public {
        require(wallet[user] >= amount, "not enough coins");

        // Transfer ETH(s) from the payer to the owner of the contract
        wallet[user] -= amount;
        wallet[owner] += amount;
    }

    /// Send an amount of ETH to be stored in the wallet
    function send() public payable {
        wallet[msg.sender] += msg.value;
    }

    /// Withdraw all ETH in the wallet.
    function withdraw() public returns (bool) {
        uint amount = wallet[msg.sender];

        require(amount > 0, "nothing to withdraw");

        // It is important to set this to zero because the recipient
        // can call this function again as part of the receiving call
        // before `send` returns.
        wallet[msg.sender] = 0;

        if (!msg.sender.send(amount)) {
            // No need to call throw here, just reset the amount owing
            wallet[msg.sender] = amount;
            return false;
        }

        return true;
    }
}
