//solium-disable linebreak-style
pragma solidity >=0.6.3 <0.7.0;

contract Ownable {

    address owner;

    modifier ownerOnly() {
        require(msg.sender == owner, "Only the owner can access this");

        _;
    }

    function changeOwner(address newOwnerAddress) public ownerOnly {
        owner = newOwnerAddress;
    }

}