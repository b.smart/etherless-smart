// networks.js
const { projectId, mnemonic, awsUrl } = require('./secrets.json');
const HDWalletProvider = require('@truffle/hdwallet-provider');

module.exports = {
  networks: {
    ropsten: {
      provider: () => new HDWalletProvider(
        mnemonic, `https://ropsten.infura.io/v3/${projectId}`
      ),
      networkId: 3,
      gasPrice: 10e9
    },
    rinkeby: {
      provider: () => new HDWalletProvider(
        mnemonic, `https://rinkeby.infura.io/v3/${projectId}`
      ),
      networkId: 4,
      gasPrice: 10e9
    },
    ganache: {
      host: 'ganache-cli',
      port: 8545,
      network_id: "*"
    },
    EC2_ganache: {
      host: '18.156.118.105',
      port: 8545,
      network_id: "*"
    }
   
  }
};